module.exports = {
  entry:  [ "./app.js" ],
  //entry:  ["./no_require.js", "./app.js", "file?name=test.html!jade-html!./test.jade"],
  output: {
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.es6$/, exclude: /node_modules/, loader: "babel-loader" },
      { test: /\.styl$/, exclude: /node_modules/, loader: "style!css!stylus" },
      //{ test: /\.styl$/, exclude: /node_modules/, loader: "style-loader!css-loader!stylus-loader" },
      { test: /\.jade$/, exclude: /node_modules/, loader: 'jade-html' },
    ]
  },
  resolve: {
    extensions: ['', '.js', '.es6', '.styl', '.jade']
  }
};
